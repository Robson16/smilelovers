<?php

/**
 * The template for displaying pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 */

get_header();

while (have_posts()) {
	the_post();
	get_template_part('partials/content/content', 'page');
}

get_footer();

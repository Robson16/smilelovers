// Define a constant for the viewport width threshold
const MOBILE_VIEWPORT_THRESHOLD = 992;

export default class Header {
	constructor() {
		this.body = document.querySelector('body');
		this.wpAdminBar = document.querySelector('#wpadminbar');
		this.header = document.querySelector('#header');
		this.navbar = document.querySelector('#navbar');
		this.navbarWrapper = this.navbar?.querySelector('.navbar-wrapper');
		this.navbarCollapseMobile = this.navbar?.querySelector(
			'.navbar-collapse-mobile'
		);

		this.navbarNavMobile =
			this.navbarCollapseMobile?.querySelector('.navbar-nav') || null;

		this.wpAdminBarHeight = 0;
		this.headerHeight = 0;
		this.navbarHeight = 0;
		this.offset = 0;

		this.viewportWidth = document.documentElement.clientWidth;
		this.viewportHeight = document.documentElement.clientHeight;

		this.events();
	}

	events() {
		if (this.navbar) {
			['resize', 'load'].forEach((event) => {
				window.addEventListener(event, () => {
					this.handleSizesReCalc();
					this.handleLoggedIn();
					this.handleNavbarCollapseSize();
				});
			});

			window.addEventListener('scroll', () => {
				this.handleStickyEffect();
			});

			this.handleNavbarToggle();
		}
	}

	handleSizesReCalc() {
		this.wpAdminBarHeight = this.wpAdminBar?.offsetHeight ?? 0;
		this.navbarHeight = this.navbarWrapper?.offsetHeight ?? 0;
		this.viewportWidth = document.documentElement.clientWidth;
		this.viewportHeight = document.documentElement.clientHeight;
		this.offset = this.wpAdminBarHeight + this.navbarHeight;
	}

	handleNavbarCollapseSize() {
		const mobileNavbarMargin = 30; // Adjust this value as needed

		if (this.navbarNavMobile) {
			// Set the margin-top based on viewport width
			this.navbarNavMobile.style.marginTop =
				this.viewportWidth < MOBILE_VIEWPORT_THRESHOLD
					? `${
							this.navbarHeight +
							this.navbar.offsetTop +
							mobileNavbarMargin
						}px`
					: null;
		}
	}

	handleLoggedIn() {
		const minNavbarTopSpace = 30;

		this.navbarWrapper.style.top = this.wpAdminBar
			? `${this.wpAdminBarHeight + minNavbarTopSpace}px`
			: `${minNavbarTopSpace}px`;
	}

	handleStickyEffect() {
		const isSticky = window.scrollY > this.offset;

		this.navbar.classList.toggle('is-sticky', isSticky);
	}

	handleNavbarToggle() {
		const navbarToggler = this.navbar?.querySelector('.navbar-toggler');

		if (navbarToggler) {
			const icon = navbarToggler.querySelector('.navbar-toggler-icon');
			const targetSelector = navbarToggler.dataset.target;
			const target = document.querySelector(targetSelector);

			if (target) {
				navbarToggler.addEventListener('click', () => {
					this.body.classList.toggle('no-scroll-vertical');

					navbarToggler.classList.toggle('show');
					target.classList.toggle('show');
					icon.classList.toggle('close');
				});
			}
		}
	}
}

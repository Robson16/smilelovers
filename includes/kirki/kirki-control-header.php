<?php

/**
 * Kirki Customizer - header
 *
 */

new \Kirki\Panel(
	'smilelovers_panel_header',
	array(
		'title'       => esc_html__('Header', 'smilelovers'),
		'description' => esc_html__('Extra options to customize the header.', 'smilelovers'),
		'priority'    => 160,
	)
);

// CTA
new \Kirki\Section(
	'smilelovers_section_header_cta',
	array(
		'title'       => esc_html__('CTA', 'smilelovers'),
		'description' => esc_html__('Call To Action Button', 'smilelovers'),
		'panel'       => 'smilelovers_panel_header',
		'priority'    => 160,
	)
);

new \Kirki\Field\Text(
	array(
		'settings' => 'smilelovers_setting_header_cta_text',
		'label'    => esc_html__('CTA - Text', 'smilelovers'),
		'section'  => 'smilelovers_section_header_cta',
		'default'  => 'SmileClub',
		'priority' => 10,
	)
);

new \Kirki\Field\URL(
	array(
		'settings' => 'smilelovers_setting_header_cta_url',
		'label'    => esc_html__('CTA - URL', 'smilelovers'),
		'section'  => 'smilelovers_section_header_cta',
		'default'  => '',
		'priority' => 10,
	)
);

new \Kirki\Field\Checkbox_Switch(
	array(
		'settings'    => 'smilelovers_setting_header_cta_target',
		'label'       => esc_html__('Open link in new page?', 'smilelovers'),
		'section'     => 'smilelovers_section_header_cta',
		'default'     => 'off',
		'choices'     => [
			'on'  => esc_html__('Yes', 'smilelovers'),
			'off' => esc_html__('No', 'smilelovers'),
		],
		'priority'    => 10,
	)
);

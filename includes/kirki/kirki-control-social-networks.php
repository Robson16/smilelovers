<?php

/**
 * Kirki Customizer - List of Social Networks
 *
 */

new \Kirki\Section(
	'smilelovers_section_social_networks',
	array(
		'title'       => esc_html__('Social Networks', 'smilerlovers'),
		'description' => esc_html__('Social networks to use around the site.', 'smilerlovers'),
		'priority'    => 160,
	)
);

new \Kirki\Field\URL(
	array(
		'settings' => 'smilelovers_setting_social_linkedin',
		'label'    => esc_html__('LinkedIn', 'smilerlovers'),
		'section'  => 'smilelovers_section_social_networks',
		'default'  => 'https://www.linkedin.com/',
		'priority' => 10,
	)
);

new \Kirki\Field\URL(
	array(
		'settings' => 'smilelovers_setting_social_instagram',
		'label'    => esc_html__('Instagram', 'smilerlovers'),
		'section'  => 'smilelovers_section_social_networks',
		'default'  => 'https://www.instagram.com/',
		'priority' => 10,
	)
);

new \Kirki\Field\URL(
	array(
		'settings' => 'smilelovers_setting_social_whatsapp',
		'label'    => esc_html__('Whatsapp', 'smilerlovers'),
		'section'  => 'smilelovers_section_social_networks',
		'default'  => 'https://api.whatsapp.com/send?phone=5511988887777&text=Ol%C3%A1',
		'priority' => 10,
	)
);

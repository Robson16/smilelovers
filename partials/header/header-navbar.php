<?php

/**
 * Header Main Navbar
 *
 */

?>

<nav id="navbar" class="navbar">
	<div class="navbar-wrapper">
		<span class="navbar-brand">
			<?php
			if (has_custom_logo()) {
				the_custom_logo();
			} else {
				echo '<h1 class="site-title" style="margin: 0;">' . get_bloginfo('title') . '</h1>';
			}
			?>
		</span>

		<?php if (has_nav_menu('main_menu')) : ?>
			<button type="button" class="navbar-toggler" data-target="#mobile-menu">
				<span class="navbar-toggler-icon">
					<div class="bar1"></div>
					<div class="bar2"></div>
					<div class="bar3"></div>
				</span>
			</button>

			<?php
			wp_nav_menu(array(
				'theme_location'      => 'main_menu',
				'depth'               => 2,
				'container'           => 'div',
				'container_class'     => 'navbar-collapse navbar-collapse-desktop',
				'container_id'        => 'desktop-menu',
				'menu_class'          => 'navbar-nav',
				'fallback_cb'         => 'WP_Bootstrap_Navwalker::fallback',
				'walker'              => new WP_Bootstrap_Navwalker()
			));
			?>
		<?php endif; ?>

		<?php
		$header_cta_text = get_theme_mod('smilelovers_setting_header_cta_text');
		$header_cta_url = get_theme_mod('smilelovers_setting_header_cta_url', '');
		$header_cta_target = get_theme_mod('smilelovers_setting_header_cta_target');
		if (!empty($header_cta_url)) :
		?>
			<a class="cta" href="<?php echo esc_url($header_cta_url); ?>" target="<?php echo ($header_cta_target) ? '_target' : '_self'; ?>">
				<?php echo esc_html($header_cta_text); ?>
			</a>
		<?php endif; ?>

		<?php
		$instagramUrl = get_theme_mod('smilelovers_setting_social_instagram');
		if (!empty($instagramUrl)) :
		?>
			<a href="<?php echo esc_html($instagramUrl); ?>" class="social-media-icon icon-instagram" target="_blank">
				<img src="<?php echo get_template_directory_uri() . '/assets/images/icon-instagram.png'; ?>" alt="Instagram">
			</a>
		<?php endif; ?>
	</div>
	<!-- /.navbar-wrapper -->

	<?php
	if (has_nav_menu('main_menu')) {
		wp_nav_menu(array(
			'theme_location'      => 'main_menu',
			'depth'               => 2,
			'container'           => 'div',
			'container_class'     => 'navbar-collapse navbar-collapse-mobile',
			'container_id'        => 'mobile-menu',
			'menu_class'          => 'navbar-nav',
			'fallback_cb'         => 'WP_Bootstrap_Navwalker::fallback',
			'walker'              => new WP_Bootstrap_Navwalker()
		));
	}
	?>
</nav>
<!-- /.navbar -->
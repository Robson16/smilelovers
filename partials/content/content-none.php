<?php
/*
 * Template Part to display that no posts were found
 */
?>

<div>
	<h2 class="text-center"><?php esc_html_e('No content to display', 'tucan'); ?></h2>
</div>
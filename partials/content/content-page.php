<?php

/**
 * Generic template part to display publication
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 */

?>

<div id="post-<?php the_ID(); ?>" <?php post_class('entry-content'); ?>>
	<?php the_content(); ?>
</div><!-- #post-<?php the_ID(); ?> -->
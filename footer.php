<?php

/**
 * The template for displaying the footer
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */
?>

</main>

<footer id="footer" class="footer">
	<?php
	$whatsappUrl = get_theme_mod('smilelovers_setting_social_whatsapp');
	if (!empty($whatsappUrl)) :
	?>
		<a href="<?php echo esc_html($whatsappUrl); ?>" class="whatsapp-button" target="_blank">
			<img src="<?php echo get_template_directory_uri() . '/assets/images/icon-whatsapp.png'; ?>" alt="WhatsApp">
		</a>
	<?php endif; ?>

	<div class="container">
		<?php if (is_active_sidebar('smilelovers-sidebar-footer-1')) : ?>
			<div class="widget-column">
				<?php dynamic_sidebar('smilelovers-sidebar-footer-1'); ?>
			</div>
		<?php endif; ?>
		<?php if (is_active_sidebar('smilelovers-sidebar-footer-2')) : ?>
			<div class="widget-column">
				<?php dynamic_sidebar('smilelovers-sidebar-footer-2'); ?>
			</div>
		<?php endif; ?>
	</div>
	<!-- /.container -->
</footer>

<?php wp_footer(); ?>

</body>

</html>
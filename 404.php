<?php

/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 */
get_header();
?>

<div class="container-fluid">
	<div class="error-message">
		<span>404</span>
		<h1 class="page-title"><?php _e('The page you are looking for does not exist.', 'smilelovers'); ?></h1>
		<a class="button" href="<?php echo get_home_url(); ?>"><?php _e('Return to home page', 'smilelovers'); ?></a>
	</div>
</div>

<?php
get_footer();

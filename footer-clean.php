<?php

/**
 * The template for displaying the footer clean
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */
?>

</main>

<?php wp_footer(); ?>

</body>

</html>
<?php

/**
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */

// Front-End
function smilelovers_scripts()
{
	// CSS
	wp_enqueue_style('smilelovers-shared-styles', get_template_directory_uri() . '/assets/css/shared/shared-styles.css', array(), wp_get_theme()->get('Version'));
	wp_enqueue_style('smilelovers-frontend-styles', get_template_directory_uri() . '/assets/css/frontend/frontend-styles.css', array(), wp_get_theme()->get('Version'));

	// Js
	wp_enqueue_script('comment-reply');
	wp_enqueue_script('smilelovers-frontend-script', get_template_directory_uri() . '/assets/js/frontend/frontend.min.js', array(), wp_get_theme()->get('Version'), true);
}
add_action('wp_enqueue_scripts', 'smilelovers_scripts');

/**
 * Set theme defaults and register support for various WordPress features.
 */
function smilelovers_setup()
{
	// Enabling translation support
	load_theme_textdomain('smilelovers', get_template_directory() . '/languages');

	// Customizable logo
	add_theme_support('custom-logo', array(
		'width'       => 121,
		'height'      => 27,
		'flex-width'  => true,
		'flex-height' => true,
		'header-text' => array('site-title', 'site-description'),
	));

	// Menu registration
	register_nav_menus(array(
		'main_menu' => esc_html__('Main Menu', 'smilelovers'),
	));

	// Load custom styles in the editor.
	add_theme_support('editor-styles');
	add_editor_style(get_stylesheet_directory_uri() . '/assets/css/shared/shared-styles.css');

	// Let WordPress manage the document title.
	add_theme_support('title-tag');

	// Enable support for featured image on posts and pages.
	add_theme_support('post-thumbnails');

	// Enable support for embedded media for full weight
	add_theme_support('responsive-embeds');

	// Standard style for each block.
	add_theme_support('wp-block-styles');
}
add_action('after_setup_theme', 'smilelovers_setup');

/**
 * Registration of widget areas.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function smilelovers_sidebars()
{
	// Args used in all calls register_sidebar().
	$shared_args = array(
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
		'before_widget' => '<div class="widget %2$s"><div class="widget-content">',
		'after_widget' => '</div></div>',
	);

	// Footer #1
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Footer #1', 'smilelovers'),
		'id' => 'smilelovers-sidebar-footer-1',
		'description' => __('The widgets in this area will be displayed in the first column in the footer.', 'smilelovers'),
	)));

	// Footer #2
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Footer #2', 'smilelovers'),
		'id' => 'smilelovers-sidebar-footer-2',
		'description' => __('The widgets in this area will be displayed in the second column in the footer.', 'smilelovers'),
	)));
}
add_action('widgets_init', 'smilelovers_sidebars');


/**
 * Register Custom Navigation Walker
 */
function smilelovers_register_navwalker()
{
	require_once get_template_directory() . '/includes/classes/class-wp-bootstrap-navwalker.php';
}
add_action('after_setup_theme', 'smilelovers_register_navwalker');

/**
 * TGM Plugin Activation
 */
require_once get_template_directory() . '/includes/required-plugins.php';

/**
 *  Kirki Customizer Framework  Config
 */
require_once get_template_directory() . '/includes/kirki/kirki-config.php';
